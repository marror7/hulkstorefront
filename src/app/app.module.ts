import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrandModule } from './brand/brand.module';
import { ProductModule } from './product/product.module';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule, NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrandModule,
    ProductModule,
    HttpClientModule,
    NgbModule,
    AngularFontAwesomeModule,
    FormsModule,
  ],
  providers: [NgbModal, NgbActiveModal],
  bootstrap: [AppComponent]
})
export class AppModule { }
