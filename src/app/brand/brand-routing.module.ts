import { CreateBrandComponent } from './create-brand/create-brand.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [{
  path: '',
  redirectTo: 'crear',
  pathMatch: 'full'
},
{
  path: 'crear',
  component: CreateBrandComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BrandRoutingModule { }
