import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit, Input } from '@angular/core';
import { ProductModel } from 'src/shared/models/product-model';
import { CrudServiceService } from 'src/shared/cruds/crud-service.service';
import Swal from 'sweetalert2';
import { ProductTypeModel } from 'src/shared/models/product-type-model';
import { ProductManageModel } from 'src/shared/models/product-manage';

@Component({
  selector: 'app-manage-stock',
  templateUrl: './manage-stock.component.html',
  styleUrls: ['./manage-stock.component.scss']
})
export class ManageStockComponent implements OnInit {
  @Input() productModelInfo: ProductModel = new ProductModel();
  @Input() ProductManageModelInfo: ProductManageModel = new ProductManageModel();

  constructor(
    public crudService: CrudServiceService,
    public activeModal: NgbActiveModal,
  ) { }

  ngOnInit() {
  }
  onSubmit() {
    debugger;
    this.ProductManageModelInfo.product = this.productModelInfo;
    this.ProductManageModelInfo.quantity = this.productModelInfo.quantity;
    this.crudService.createModel('api/producto/agregar-productos', this.ProductManageModelInfo).subscribe(
    data => {
      Swal.fire({
        title: 'Exito!',
        text: 'Stock agregado',
        icon: 'success',
        confirmButtonText: 'Continuar'
      });
      this.ngOnInit();
    }
  );
}

}
