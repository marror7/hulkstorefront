import { ProductManageModel } from './../../../shared/models/product-manage';
import { async } from '@angular/core/testing';
import { ManageStockComponent } from './../manage-stock/manage-stock.component';
import { ProductModel } from './../../../shared/models/product-model';
import { CrudServiceService } from './../../../shared/cruds/crud-service.service';
import { Component, OnInit, NgModule } from '@angular/core';
import swal from 'sweetalert2';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';



@Component({
  selector: 'app-list-product',
  templateUrl: './list-product.component.html',
  styleUrls: ['./list-product.component.scss']
})
export class ListProductComponent implements OnInit {

  rows: Array<ProductModel> = new Array();
  columns: any = [];
  filter = '';
  newProductManage: ProductManageModel = new ProductManageModel();
  constructor(
    public crudService: CrudServiceService,
    public modal: NgbModal) { }

  ngOnInit() {
    this.getProducts();
  }

  getProducts() {
    this.crudService.getModel('api/producto/buscar-productos').subscribe(
      data => {
        this.rows = data;

      }
    );
  }
  getAlert() {
    swal.fire({
      title: 'Error!',
      text: 'Do you want to continue',
      icon: 'error',
      confirmButtonText: 'Cool'
    });
  }

  openModal() {
    this.modal.open(ManageStockComponent, { windowClass: 'modal', backdrop: 'static', size: 'lg' });
  }

  filterProducts() {
    console.log(this.filter);
    this.crudService.getModel('api/producto/filtrar-productos?filter=' + this.filter).subscribe(
      data => {
        this.rows = data;
      });
  }


  addMessage(product: ProductModel) {
    (async () => {
      const { value: text } = await Swal.fire({
        title: 'Agregar Stock!',
        text: 'Ingresa un valor de 1 a 100 unidades',
        icon: 'info',
        confirmButtonText: 'Agregar',
        input: 'number',
        inputAttributes: {
          min: '1',
          max: '100'
        },
        inputValue: '1'
      });
      if (text) {
        this.newProductManage.product.id = product.id;
        this.newProductManage.quantity = text;
        this.AddStock(this.newProductManage);
      }
    })();
  }

  AddStock(newProductManage: ProductManageModel) {
    this.crudService.createModel('api/producto/agregar-productos', newProductManage).subscribe(
      data => {
        swal.fire({
          title: 'Exito!',
          text: 'Stock agregado',
          icon: 'success',
          confirmButtonText: 'Continuar'
        });
        this.ngOnInit();
      }
    );

  }

  removeMessage(product: ProductModel) {
    (async () => {
      const { value: text } = await Swal.fire({
        title: 'Restar a Stock!',
        text: 'Ingresa un valor de 1 a ' + product.quantity + ' unidades',
        icon: 'info',
        confirmButtonText: 'Restar',
        input: 'number',
        inputAttributes: {
          min: '1',
          max: product.quantity + ''
        },
        inputValue: '1'
      });

      if (text > product.quantity) {
        swal.fire('ups la cantidad supera al stock porfavor verifica');
      }
      if (text) {
        this.newProductManage.product.id = product.id;
        this.newProductManage.quantity = text;
        this.removeStock(this.newProductManage);
      }
    })();
  }

  removeStock(newProductManage: ProductManageModel) {
    this.crudService.createModel('api/producto/restar-productos', newProductManage).subscribe(
      data => {
        swal.fire({
          title: 'Exito!',
          text: 'Stock restado',
          icon: 'success',
          confirmButtonText: 'Continuar'
        });
        this.ngOnInit();
      }
    );

  }
}
