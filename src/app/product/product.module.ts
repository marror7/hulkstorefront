import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ProductRoutingModule } from './product-routing.module';
import { ListProductComponent } from './list-product/list-product.component';
import { ManageStockComponent } from './manage-stock/manage-stock.component';


@NgModule({
  declarations: [ListProductComponent, ManageStockComponent],
  imports: [
    CommonModule,
    ProductRoutingModule,
    NgxDatatableModule,
    FormsModule,
    ReactiveFormsModule

  ],
  entryComponents: [ManageStockComponent]
})
export class ProductModule { }
