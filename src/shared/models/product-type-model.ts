export class ProductTypeModel {
  id: string;
  name: string;
  createdDate: Date;

  public constructor() {
    this.id = '';
    this.name = '';
    this.createdDate = null;
  }
}
