import { ProductModel } from './product-model';
export class ProductManageModel {
  product: ProductModel;
  quantity: number;

  public constructor() {
    this.product = new ProductModel();
    this.quantity = 0;
  }
}
